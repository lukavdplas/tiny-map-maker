local constants = require "utils.constants"
local cells = require "utils.cells"
local draw = require "utils.draw"
local buildings = require "utils.buildings"
local update = require "utils.update"
local tick = require "utils.tick"
local hud = require "utils.hud"
local test = require "test.test"

local grid
-- building mode
local mode = "house"
-- Y coordinate at which the HUD starts
local hudStart

function love.load()
    hudStart = love.graphics.getHeight() - constants.hudSize
    grid = cells.newGrid()
    tick.reset()
    draw.init()
end

function love.update(dt)
    if tick.tick() then
        update.updateGreenery(grid)
    end
end

function love.draw()
    draw.drawGrid(grid)
    draw.drawHighlight(update.hoveredCoordinates(grid))
    draw.drawHud(mode)
end

function love.keypressed(key)
    if key == 'escape' then
       love.event.quit()
    end

    if key == 't' then
        test.test()
    end
end

function love.mousepressed(x, y, button)
    if y < hudStart then
        buildings.toggleAtCoordinates(grid, x, y, mode)
    else
        mode =  hud.clickedMode(x, y, mode)
    end
end