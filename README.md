# Tiny map maker

This is a small project I made to try out the [LÖVE framework](https://love2d.org/) for making games. The result is a small "map maker" where you plan place houses, roads, and trees.

![screenshot showing a 2D map with houses, roads, and trees, and a toolbar at the bottom](/screenshot.png)

## Installation

I don't have an executabe of the game available, but you can run it by installing LÖVE. This game was developed in LÖVE 11.4. The [LÖVE wiki](https://love2d.org/wiki/Getting_Started) includes a getting started guide for different platforms.

### Windows 10

I develop in Windows 10. Since it took me a second to get my environment set up, I've written it down for future reference.

- Download the installer from [love2d.org](https://love2d.org/#download) and install LÖVE.
- Open Powershell and navigate to the folder where you installed love. By default, this is `C:\Program Files\LOVE`
- Running `.\love` now runs the love executable. `.\love --version` should tell you the version. Run `.\love C:\path\to\repository\` to run this program.
- If you want, you can configure your `PATH` environment variable to include `.`, which allows you to run the above commands as `love` (without `.\`).

For developing in VSCode, the `love-launcher` extension works well for me, and allows you to run your program with `Alt + L`

## How to play

The game must be played with a mouse.

- Click on a square on the map to place something there. Click again to clear it.
- The toolbar at the bottom allows you to pick between placing houses, roads and trees.
- Roads on adjacent squares connect automatically.
- If you remove a house from a square and place a new house there, you may get a different picture. The same goes for trees.

## Licence

All works in this repository © Luka van der Plas, 2024.

This repository is shared under the following two licences:

- [BSD-3-Clause](/LICENCES/BSD-3-Clause)
- [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/) ([plain-text copy](/LICENCES/CC-BY-4.0))

If you want to re-use the work in this repository, you are free to choose one of these licences and re-use it under those terms.

Note that this repository does *not* include LÖVE, which is necessary to run the game. If you wish to reuse a version that includes the framework, you will have to abide by LÖVE's licencing terms as well. LÖVE is shared under a zlib/libpng licence; see [LÖVE's licence statement](https://github.com/love2d/love/blob/11.4/license.txt) for details information.
