local images = require "utils.images"

local function testHouseImagesLoaded()
    assert(# images.house >= 1)
end

local function testTreeImagesLoaded()
    assert(# images.tree >= 1)
end

return {
    houseImagesLoaded = testHouseImagesLoaded,
    treeImagesLoaded = testTreeImagesLoaded,
}