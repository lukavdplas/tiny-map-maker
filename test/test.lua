local test_images = require "test.test_images"
local test_road = require "test.test_road"

local test_modules = {test_images, test_road}

local function testModule(resultsTable, module)
    for name, unittest in pairs(module) do
        local success, res = pcall(unittest)
        if not success then
            print("FAILED: "..name)
            print(res)
        else
            print("SUCCESS")
        end
        table.insert(resultsTable, success)
    end
    return resultsTable
end

-- execute all unittests
local function test()
    print('Running unit tests...')
    local results = {}

    for i, module in ipairs(test_modules) do
        results = testModule(results, module)
    end
    
    print("Done! Executed " .. #results .. " unit tests")
end

return {
    test = test
}