local road = require "utils.road"

local grid = {
    {
        { building = "road" },
        { building = nil }
    },
    {
        { building = "road" },
        { building = nil }
    }
}

local function testCheckAdjacentRoads()
    local result = road._checkAdjacentRoads(1, 1, grid[1][1], grid)
    local expected = { true, true, false, true }

    for i, value in ipairs(result) do
        local expectedValue = expected[i]
        assert(value == expectedValue)
    end
end

local function testBinaryTableToString()
    local input = {true, false, false, true, false}
    local result = road._binaryTableToString(input)
    assert(result == "10010")
end

local function testBinaryTableToInt()
    local input = {true, false, true }
    assert(road._binaryTableToInt(input) == 5)

    input = {true, true, false, true}
    assert(road._binaryTableToInt(input) == 13)
end

local function testAdjacentRoadsToRoadType()
    local input = {true, false, false, true}
    local name, rotations = road._adjacentRoadsToRoadType(input)
    assert(name == "1100")
    assert(rotations == 3)
end

return {
    checkAdjacentRoads = testCheckAdjacentRoads,
    binaryTableToString = testBinaryTableToString,
    binaryTableToInt = testBinaryTableToInt,
    adjacentRoadsToRoadType = testAdjacentRoadsToRoadType,
}