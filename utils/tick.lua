local tickLength = 1.0
local start
local currentTick

local function reset()
    start = love.timer.getTime()
    currentTick = 1
end

-- update the current tick based on time passed.
-- returns true if the current tick is updated
local function tick()
    local timePassed = love.timer.getTime() - start
    local tick = math.ceil(timePassed / tickLength)
    if tick ~= currentTick then
        currentTick = tick
        return true
    else
        return false
    end
end

return {
    reset = reset,
    tick = tick,
}