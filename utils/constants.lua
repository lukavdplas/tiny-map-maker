return {
    -- thresholds levels for drawing greenery objects (shrubs)
    greeneryDrawThresholds = {0.2, 0.6},
    -- probability that a shrub will spawn at a sufficient greenery level
    greenerySpawnChance = 0.5,
    -- cell size in pixels
    cellSize = 50,
    -- height of the HUD in pixels
    hudSize = 50,
    -- building modes
    modes = {"house", "tree", "road"}
}