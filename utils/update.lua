local constants = require "utils.constants"
local cells = require "utils.cells"

local nGreenery = # love.filesystem.getDirectoryItems("assets/greenery/tier1")

-- the grid coordinates of the cell that the mouse is hovering
local function hoveredCoordinates(grid)
    local mouse_x, mouse_y = love.mouse.getPosition()
    return cells.gridCoordinates(mouse_x, mouse_y, grid)
end

-- coordinates of all potential neighbours for a cell
-- this may include out-of-bounds coordinates
local function neighborhoodCoordinates(i, j)
    return {
        {i-2, j},
        {i-1, j-1},
        {i-1, j},
        {i-1, j+1},
        {i, j-2},
        {i, j-1},
        {i, j+1},
        {i, j+2},
        {i+1, j-1},
        {i+1, j},
        {i+1, j+1},
        {i+2, j},
    }
end

-- returns a table with the neighbourhood of a cell
-- each item is a tiple of i, j, state - where the state may be nil
-- if (i,j) is not on the grid
local function cellNeighborhood(i, j, state, grid)
    local coordinates = neighborhoodCoordinates(i, j)
    local neighborhood = {}

    for _, cors in ipairs(coordinates) do
        local n_i, n_j = cors[1], cors[2]
        if grid[n_i] ~= nil and grid[n_i][n_j] ~= nil then
            table.insert(neighborhood, {n_i, n_j, grid[n_i][n_j]})
        else
            table.insert(neighborhood, {n_i, n_j, nil})
        end
    end

    return neighborhood
end

-- distance between two cells
local function proximity(i_1, j_1, i_2, j_2)
    local distance = math.sqrt(
        (i_1 - i_2)^2 + (j_1 - j_2)^2
     )

     return 1 / distance
end

local function calculateGreenery(i, j, state, grid)
    local greenery

    -- initial bonus if there is a tree on this cell
    if state.building == "tree" then
        greenery = 0.5
    else
        greenery = 0
    end

    -- update further based on surrounding trees
    local neighborhood = cellNeighborhood(i, j, state, grid)

    for _, cell in ipairs(neighborhood) do
        local c_i, c_j, c_state = cell[1], cell[2], cell[3]
        if c_state ~= nil and c_state.building == "tree" then
            greenery = greenery + 0.2 * proximity(i, j, c_i, c_j)
        end
    end

    greenery = math.min(greenery, 1) -- cap out at 1

    return greenery
end

-- returns True with random chance p
local function randomChance(p)
    return math.random() < p
end

-- set the greenery instance of a cell state, if it needs to be updated
local function setGreenery(state, greenery)
    local isEmpty = state.building == nil
    local noGreeneryObject = state.greenery < constants.greeneryDrawThresholds[1]
    local enoughGreenery = greenery >= constants.greeneryDrawThresholds[1]

    if isEmpty and noGreeneryObject and enoughGreenery then
        if randomChance(constants.greenerySpawnChance) then
            state.instance = math.random(nGreenery)
        else
            state.instance = nil
        end
    end

    state.greenery = greenery
end

-- update greenery state of a cell
local function updateCellGreenery(i, j, state, grid)
    local greenery = calculateGreenery(i, j, state, grid)
    setGreenery(state, greenery)
end

local function updateGreenery(grid)
    cells.forEachCell(updateCellGreenery, grid)
end

-- updates to cell states
return {
    hoveredCoordinates = hoveredCoordinates,
    updateGreenery = updateGreenery,
}