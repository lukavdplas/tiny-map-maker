local constants = require "utils.constants"

local margin = 5

-- the bouding X/Y coordinates of a button based on its position + mode.
local function buttonArea(i, mode)
    local width, height = love.graphics.getDimensions()
    local hudTop = height - constants.hudSize
    local buttonsRight = (#constants.modes) - i
    local buttonSize = constants.hudSize - (margin * 2)
    local y1 = hudTop + margin
    local y2 = y1 + buttonSize
    local x2 = width - (constants.hudSize * buttonsRight + margin)
    local x1 = x2 - buttonSize
    return x1, y1, x2, y2
end

-- handle a click event on the HUD.
local function clickedMode(x, y, currentMode)
    for i, mode in ipairs(constants.modes) do
        local x1, y1, x2, y2 = buttonArea(i, mode)
        if x >= x1 and x <= x2 and y >= y1 and y <= y2 then
            return mode
        end
    end
    return currentMode
end

-- manage interactions with the HUD
return {
    buttonSize = constants.hudSize - margin * 2,
    buttonArea = buttonArea,
    clickedMode = clickedMode,
}