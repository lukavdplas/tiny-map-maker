local function hasRoad(state)
    return state == nil or state.building == "road"
end

-- lists whether the adjacent cells have roads on them.
-- Thse are returned as a boolean array in N, E, S, W order.
local function checkAdjacentRoads(i, j, state, grid)
    local N = j <= 1 or hasRoad(grid[i][j-1])
    local E = i >= #grid or hasRoad(grid[i+1][j])
    local S = j >= #grid[1] or hasRoad(grid[i][j+1])
    local W = i <= 1 or hasRoad(grid[i-1][j])
    return {N, E, S, W}
end

-- shifts a table by one, e.g. {1,2,3,4} |-> {2,3,4,1}
local function shiftTable(t)
    local newTable = {}
    for i, v in ipairs(t) do
        table.insert(newTable, v)
    end
    local head = table.remove(newTable, 1)
    table.insert(newTable, head)
    return newTable
end

-- list all rotations of an adjacency table
local function listRotations(adjacencyTable)
    local rotations = {adjacencyTable}
    for i=1,#adjacencyTable-1 do
        table.insert(rotations, shiftTable(rotations[i]))
    end
    return rotations
end

-- converts a table of booleans to a number as if it is a binary number,
-- e.g. {true, false, true} |-> 4 + 0 + 1 |-> 5
local function binaryTableToInt(t)
    local total = 0
    for i, value in ipairs(t) do
        if value then
            local power = #t - i
            total = total + 2 ^power
        end
    end
    return total
end

-- converts a table of booleans to a string,
-- e.g. {true, false, true} |-> "101"
local function binaryTableToString(t)
    local s = ""
    for i, v in ipairs(t) do
        if v then
            s = s .. "1"
        else
            s = s .. "0"
        end
    end
    return s
end

-- turns a table of adjacent roads (in N, E, S, W order)
-- into a standardised representation of the road type
-- i.e. the binary string used in the image name
-- and the number of 90-degree clockwise rotations
-- needed for the image
local function adjacentRoadsToRoadType(adjacent)
    local rotations = listRotations(adjacent)
    local rotationsNeeded = 0
    local standardRotation = rotations[1]
    
    for i, v in ipairs(rotations) do
        if binaryTableToInt(v) > binaryTableToInt(standardRotation) then
            standardRotation = v
            rotationsNeeded = i - 1
        end
    end

    return binaryTableToString(standardRotation), rotationsNeeded
end

-- returns the type of road that should be rendered on a cell.
-- Output consists of two elements: the image class of the road
-- and the rotation required for the image
local function roadType(i, j, state, grid)
    if hasRoad(state) then
        local adjacent = checkAdjacentRoads(i, j, state, grid)
        return adjacentRoadsToRoadType(adjacent)
    else
        return nil, nil
    end
end

return {
    _binaryTableToInt = binaryTableToInt,
    _binaryTableToString = binaryTableToString,
    _checkAdjacentRoads = checkAdjacentRoads,
    _adjacentRoadsToRoadType = adjacentRoadsToRoadType,
    roadType = roadType
}