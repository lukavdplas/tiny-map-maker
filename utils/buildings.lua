local cells = require "utils.cells"

local nHouses = # love.filesystem.getDirectoryItems("assets/house")
local nTrees = # love.filesystem.getDirectoryItems("assets/tree")

-- toggle a tile building state
local function toggleBuilding(i, j, state, building)
    if state.building ~= building then
        state.building = building
        if building == "house" then
            state.instance = math.random(1, nHouses)
        elseif state.building == "tree" then
            state.instance = math.random(1, nTrees)
        else
            state.instance = 1
        end
    else
        state.building = nil
        state.instance = nil
    end
end

-- add a building in the grid based on some position in the window
local function toggleAtCoordinates(grid, x, y, building)
    local i, j = cells.gridCoordinates(x, y, grid)
    if i ~= nil and j ~= nil then
        toggleBuilding(i, j, grid[i][j], building)
    end
end

-- functionality for adding buildings based on user interaction
return {
    toggleAtCoordinates = toggleAtCoordinates,
 }