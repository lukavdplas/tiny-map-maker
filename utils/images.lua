local imageDirectory = "assets"

local function objectImageDirectory(object, subdirectory)
    local dir = imageDirectory .. "/" .. object
    if subdirectory ~= nil then
        dir = dir .. "/" .. subdirectory
    end
    return dir
end

-- load the images for an object
local function loadSprites(object, subdirectory)
    local dir = objectImageDirectory(object, subdirectory)
    local files = love.filesystem.getDirectoryItems(dir)
    local images = {}
    for i, filename in ipairs(files) do
        local image = love.graphics.newImage(dir.."/"..filename)
        table.insert(images, image)
    end
    return images
end

local function loadRoads()
    local dir = imageDirectory .. "/road"
    local types = love.filesystem.getDirectoryItems(dir)

    local images = {}
    for i, type in ipairs(types) do
        images[type] = loadSprites("road", type)
    end 
    return images
end

local function loadHudIcons()
    local images = {}
    local dir = imageDirectory .. "/icons"
    local files = love.filesystem.getDirectoryItems(dir)
    for i, filename in ipairs(files) do
        local image = love.graphics.newImage(dir .. "/" .. filename)
        local name = string.match(filename, "%a+")
        images[name] = image
    end
    return images
end

-- image objects
return {
    icons = loadHudIcons(),
    house = loadSprites("house"),
    tree = loadSprites("tree"),
    greenery1 = loadSprites("greenery", "/tier1"),
    greenery2 = loadSprites("greenery", "/tier2"),
    road = loadRoads(),
}