local constants = require "utils.constants"
local cells = require "utils.cells"
local images = require "utils.images"
local hud = require "utils.hud"
local road = require "utils.road"

-- initial call during love.load - sets the background colour
local function init()
    love.graphics.setBackgroundColor(0.35, 0.31, 0.25)
end

-- draw a rectangle that covers a cell's area
-- arguments are respectively the grid coordinates and the rgba values
local function fillCell(i, j, r, g, b, a)
    local x, y = cells.coordinatesToPosition(i, j)
    local size = cells.size
    love.graphics.setColor(r, g, b, a)
    love.graphics.rectangle("fill", x, y, size, size)
end

local function imageOffset(rotations)
    local offsets = {
        {0, 0},
        {0, cells.size * 2},
        {cells.size * 2, cells.size * 2},
        {cells.size * 2, 0}
    }
    for i, v in ipairs(offsets) do
        if i-1 == rotations then
            return v[1], v[2]
        end
    end
end

local function drawCellImage(i, j, image, rotations)
    local x, y = cells.coordinatesToPosition(i, j)
    local angle = rotations * 0.5 * math.pi
    local offsetX, offsetY = imageOffset(rotations)
    local scaleX = cells.size / image:getWidth()
    local scaleY = cells.size / image:getHeight()
    love.graphics.setColor(1, 1, 1)
    love.graphics.draw(image, x, y, angle, scaleX, scaleY, offsetX, offsetY)
end

-- draw a building 
local function drawObject(i, j, building, instance)
    local image = images[building][instance]
    drawCellImage(i, j, image, 0)
end

local function drawRoad(i, j, state, grid)
    local roadType, rotations = road.roadType(i, j, state, grid)
    local image = images.road[roadType][state.instance]
    drawCellImage(i, j, image, rotations)
end

-- draw cell background - indicates the greenery level
local function drawGreenery(i, j, state)
    fillCell(i, j, 0.28, 0.35, 0.18, state.greenery)

    if state.building == nil and state.greenery > constants.greeneryDrawThresholds[1] then
        local tier = 1
        if state.greenery > constants.greeneryDrawThresholds[2] then
            tier = 2
        end

        if state.instance ~= nil then
            drawObject(i, j, "greenery" .. tier, state.instance)
        end
    end
end

-- mark the (hover) highlight
local function drawHighlight(i, j)
    if i ~= nil and j ~= nil then
        fillCell(i, j, 1, 0.78, 0.15, 0.5)
    end
end

-- call all necessary drawing functions for a cell
local function drawCell(i, j, state, grid)
    drawGreenery(i, j, state)

    if state.building ~= nil then
        if state.building == "road" then
            drawRoad(i, j, state, grid)
        else
            drawObject(i, j, state.building, state.instance)
        end
    end
end

-- draw all the cells in the grid
local function drawGrid(grid)
    cells.forEachCell(drawCell, grid)
end

local function drawHudBackground()
    local windowWidth, windowHeight = love.graphics.getDimensions()
    local hudTop = windowHeight - constants.hudSize

    love.graphics.setColor(0.5, 0.5, 0.5)
    love.graphics.rectangle("fill", 0, hudTop, windowWidth, constants.hudSize)

    love.graphics.setColor(0,0,0)
    love.graphics.line(0, hudTop, windowWidth, hudTop)
end

local function drawHudButton(i, mode, isActive)
    local x1, y1, x2, y2 = hud.buttonArea(i, mode)

    if isActive then
        love.graphics.setColor(1, 1, 1, 0.25)
        love.graphics.rectangle("fill", x1, y1, hud.buttonSize, hud.buttonSize)
    end

    -- draw outline
    love.graphics.setColor(0,0,0)
    love.graphics.rectangle("line", x1, y1, hud.buttonSize, hud.buttonSize)
    

    -- draw image
    local image = images.icons[mode]
    local scaleX = hud.buttonSize / image:getWidth()
    local scaleY = hud.buttonSize / image:getHeight()
    love.graphics.setColor(1,1,1)
    love.graphics.draw(image, x1, y1, 0, scaleX, scaleY)
end

local function drawHud(currentMode)
    drawHudBackground()
    for i, mode in ipairs(constants.modes) do
        drawHudButton(i, mode, mode == currentMode)
    end
end

-- drawing functions
return {
    init = init,
    drawGrid = drawGrid,
    drawHighlight = drawHighlight,
    drawHud = drawHud,
}