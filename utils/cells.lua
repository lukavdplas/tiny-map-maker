local constants = require "utils.constants"

local size = constants.cellSize

-- initial state of a cell
local function initialState()
    return  {
        hover = false, -- whether the cell is hovered by the mouse
        greenery = 0, -- amount of greenery - influenced by nearby trees
        instance = nil, -- random instance used for image selection
    }
end

-- initialises a new grid
local function newGrid()
    local width, height = love.graphics.getDimensions()
    local rows = math.floor(width / size)
    local cols = math.floor((height - constants.hudSize) / size)

    local grid = {}

    for i=1,rows do
        local row = {}
        for j=1, cols do
            table.insert(row, initialState())
        end
        table.insert(grid, row)
    end 

    return grid
end

-- the window coordindates of the top left corner of a cell
local function coordinatesToPosition(i, j)
    local x = i * size - size
    local y = j * size - size
    return x, y
end

-- gives the grid coordinates for the given x, y coordinates in the window
local function gridCoordinates(x, y, grid)
    local i = math.ceil(x / size)
    local j = math.ceil(y / size)
    if i <= #grid and j <= # grid[1] then
        return i, j
    else
        return nil, nil
    end
end


-- apply a function to each cell in a grid
-- used for state-modifying functions: does not return a mapped grid
local function forEachCell(func, grid)
    for i, row in ipairs(grid) do
        for j, cellState in ipairs(row) do
            func(i, j, cellState, grid)
        end
    end
end

-- basic grid functionality
return {
    size = size,
    newGrid = newGrid,
    coordinatesToPosition = coordinatesToPosition,
    forEachCell = forEachCell,
    gridCoordinates = gridCoordinates,
}